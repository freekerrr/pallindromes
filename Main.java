import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws IOException {
        InputStreamReader read = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(read);

        System.out.println("Введите из скольки букв будут состоять сгенерированные слова: ");
        int size = Integer.parseInt(in.readLine());

        ArrayList<String> text = textGen(size);
        System.out.println(text);

        ArrayList<String> ArrayString = new ArrayList<>();

        for(int i=0;i<100;i++){
            String[] arr = text.get(i).split(" ");
            for(String s:arr){
                if(isPalindrome(s))
                    ArrayString.add(s);
            }
        }

        System.out.println("Палиндромы из текста:");
        System.out.println(ArrayString);
    }

    private static boolean isPalindrome(String text) {
        return text.equals(new StringBuilder(text).reverse().toString());
    }

    private static ArrayList<String> textGen(int size) {
        StringBuilder builder = new StringBuilder();
        ArrayList<String> randText = new ArrayList<>();
        for(int i=0; i<100; i++){
            builder.append("\n");
            for(int a=0; a<3; a++){
                for(int b=0; b<size; b++){
                    builder.append((char)(Math.random()*(122-97)+97));
                }
                if(a!=2) builder.append(" ");
            }
            randText.add(builder.toString());
            builder.setLength(0);
        }

        return randText;
    }
}
